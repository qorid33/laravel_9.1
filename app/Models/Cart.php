<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class Cart extends Model
{
    use HasFactory;
    protected $fillable=[
        'cashier_id','product_id','qty'
    ];

    public function products()
    {
        return $this->BelongTo(Products::class);
    }
}
