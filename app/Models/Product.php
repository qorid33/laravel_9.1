<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class Product extends Model
{
    use HasFactory;
    protected $fillable =[
        'image','barcode','title','description','buy_price','sell_price','stock'
    ];

    public function categories()
    {
        return $this->BelongTo(categories::class);
    }

    
    public function details()
    {
        return $this->HasMany(transactions_details::class);
    }
    
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value)=>asset('storage/products/'.$value)
        );
        
    }

    
}
