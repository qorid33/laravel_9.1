<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class Customer extends Model
{
    use HasFactory;
    protected $fillable=[
        'name','no_telp','address'
    ];

    public function transactions()
    {
        return $this->BelongTo(transactions::class);
    }

}
