<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class Profit extends Model
{
    use HasFactory;
    protected $fillable =[
        'transaction_id','total'
    ];

    public function transaction()
    {
        return $this->BelongTo(transaction::class);
    }

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value)=>Carbon::parse($value)->format('d-M-Y')
        );
        
    }
}
