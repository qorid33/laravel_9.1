<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class Category extends Model
{
    use HasFactory;
    protected $fillable=[
        'image','name','description'
    ];
    public function product()
    {
        return $this->HasMany(Products::class);
    }

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value)=>asset('storage/categories/'.$value)
        );
        
    }
}
