<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable=[
        'cashier_id','costumer_id','invoice','cash','change','discount','table_total'
    ];

    public function details()
    {
        return $this->HasMany(transaction_details::class);
    }

    public function costumers()
    {
        return $this->BelongTo(user::class,'cashier_id');
    }
    public function profit()
    {
        return $this->HasMany(profits::class);
    }
}
