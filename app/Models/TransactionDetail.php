<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;

class TransactionDetail extends Model
{
    use HasFactory;
    protected $fillable =[
        'transaction_id','product_id','qty','price'
    ];
    public function transactions()
    {
        return $this->BelongTo(transactions::class);
    }

    public function product()
    {
        return $this->BelongTo(Products::class);
    }
}
